from openalpr import Alpr
import sys
import time
import socket
import json
import picamera
import numpy as np
import cv2
from getServerIP import getServerIP


# openalprstuff below

def platedetection():
    alpr = Alpr("eu", "/etc/openalpr/openalpr.conf", "/usr/share/openalpr/runtime_data/")
    if not alpr.is_loaded():
        print("Error loading OpenALPR")
        sys.exit(1)

    alpr.set_top_n(20)
    alpr.set_default_region("md")
    filename = 'testpictures/patty_klein.jpg'
    camerafilename = 'testpictures/current.png'

    with picamera.PiCamera() as camera:
        camera.resolution = (640, 320)
        camera.hflip = True
        camera.vflip = True
        camera.start_preview()
        time.sleep(4)
        camera.stop_preview()
        # image = np.empty((480 * 640 * 3,), dtype=np.uint8)
        camera.capture(camerafilename)
        #cv2.imwrite(camerafilename, image)

    results = alpr.recognize_file(camerafilename)
    the_best_guess = []

    i = 0
    for plate in results['results']:
        i += 1
        print("Plate #%d" % i)
        print("   %12s %12s" % ("Plate", "Confidence"))
        x = 0
        for candidate in plate['candidates']:
            x += 1
            prefix = "-"
            if candidate['matches_template']:
                prefix = "*"

            print("  %s %12s%12f" % (prefix, candidate['plate'], candidate['confidence']))
            if (x == 1):
                the_best_guess.append(candidate['plate'])
    print("THE BEST GUESSES ARE : ", the_best_guess)
    # you can return this variable here.
    return the_best_guess


def ts():
    x = json.dumps(platedetection())
    s.send(x.encode())

host = getServerIP()

def TakePicAndSendToServer():
    
    global s
    global host
    global port

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
     #"172.20.10.8"
    port = 8888
    s.connect((host, port))

    
    #time.sleep(5)
    ts()
    print("data sent")

    #alpr.unload()
    s.close()
