# Parking Lot Control System

This is a project done for the Edge Computing and the Internet of Things practical course.

Contributors : Umur Mert Bernadotte, Simge Özcan, Philipp Reindl-Spanner, Haider Mushtaq


## 1. Project Description
The Parking Lot Control System is an IoT and edge computing based system that enables the user to monitor a
parking lot and track the duration of how long vehicles have already been parked ("overparked"). It also alerts the
user if a vehicle has been parked for too long without a valid ticket. The system is designed in such a way that it is
reliable and easy to set up. Two possible use cases will be described in the following subsections. After an insight into
the problem an overview of the solution that we created is given.

### 1.1 Problem Description
Constantly magnifying population and availability of advanced manufacturing processes cause a significant increase
in the total number of cars in major cities. This leads to parking spaces becoming more and more scarce. When it
comes to urban planning and smart cities of the future initiatives the need for smart parking space allocation and
their usage is apparent. Our project is an attempt to tackle this issue.
For business owners who own private parking areas situated nearby their establishments it is problematic if people
occupy parking lots within these private areas without being customers of their business, especially when they park for
extended periods of time. This leads to actual customers being unable to find a parking spot and therefore having to
walk long distances to reach the shop, or even worse it can lead to people getting frustrated with the parking situation
and deciding to go somewhere else instead. Situations like these harm and hinder business development as well as
revenue due to loss of customers.
Another use case for this system is automated cash-free parking payment. In Germany and most other countries
around the world the way parking works is that one parks their car in a lot and then walks to an automatic vendor
machine that provides a parking voucher to be put beneath the windscreen. The machine usually only accepts coins
and prints parking vouchers valid for a set amount of time (in the inner city of Munich this corresponds to 30 cents
for 6 minutes). In order to avoid fines or penalties people usually take precautionary measures by paying extra. They
have to estimate the amount of time they are planning to park their car. By registering their license plate with a
company that has the herein presented system installed, users will be able to have their parking time calculated and
(if desired) automatically charged using previously provided payment information.

### 1.2 Concept
To solve the previously described problem a system needs to be designed that captures the usage of parking spaces.
Our system is designed to take pictures of a parking lot directly in front of the camera so that the registration plate
can be captured. By identifying the car’s registration through computer vision the system then can distinctly assign
the parking space to that registration. In addition, the system can monitor the time that the car has already spent
on the parking space by re-checking the parking situation on that specific space. With this system business owners
can be informed which parking lots have been occupied for too long without a valid ticket. Also, evidence in form of
a picture of the car’s registration can be provided which may be used for reporting the car owner to the police or to
a towing company.

## 2. Equipment
After several iterations and implementations we finally figured out the best way to realise this system. The following
devices are needed for our final setup:  

• 3 x Raspberry Pi 3  
• 2 x Raspberry Pi Camera Module  
• 2 x Ultrasonic Distance Sensor  

Note: While our system is capable of handling multiple devices we downgraded to just one parking lot device in the
final presentation due to time, room and license plate limitations.
