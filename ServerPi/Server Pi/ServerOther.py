import mysql.connector
from mysql.connector import Error
import json
import socket
from threading import *
import datetime


def insertVariblesIntoTable(LicensePlate, ParkingStatus, ParkingStart, ParkingEnd, LotID):
    try:
        connection = mysql.connector.connect(host='localhost',
                                             database='ParkingLotSystem',
                                             user='parkinglot',
                                             password='edgecomputing')
        cursor = connection.cursor()
        mySql_insert_query = """INSERT INTO Cars (LicensePlate, ParkingStatus, ParkingStart, ParkingEnd, LotID) 
                                VALUES (%s, %s, %s, %s, %s) """

        recordTuple = (LicensePlate, ParkingStatus, ParkingStart, ParkingEnd, LotID)
        cursor.execute(mySql_insert_query, recordTuple)
        connection.commit()
        print("insert")

    except mysql.connector.Error as error:
        print("Failed to insert into MySQL table {}".format(error))

    finally:
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            print("MySQL connection is closed")


def setparking(LicensePlate, LotID):
    try:
        connection = mysql.connector.connect(host='localhost',
                                             database='ParkingLotSystem',
                                             user='parkinglot',
                                             password='edgecomputing')
        cursor = connection.cursor()

        ParkingStart = datetime.datetime.now()
        ParkingStatus = 1

        mySql_insert_query = "UPDATE Cars SET LicensePLate = %s, ParkingStatus = 1, ParkingStart = %s, Overparked = 0 WHERE LotID = 1"  

        recordTuple = (LicensePlate, ParkingStart)
        cursor.execute(mySql_insert_query, recordTuple)
        connection.commit()
        print("set")

    except mysql.connector.Error as error:
        print("Failed to insert into MySQL table {}".format(error))

    finally:
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            print("MySQL connection is closed")


def getparking(LotID):
    try:
        connection = mysql.connector.connect(host='localhost',
                                             database='ParkingLotSystem',
                                             user='parkinglot',
                                             password='edgecomputing')

        cursor = connection.cursor()
        cursor.execute('SELECT * FROM Cars WHERE LotID=1')
        records = cursor.fetchall()
        return records

    except Error as e:
        print("Error reading data from MySQL table", e)

    finally:
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            print("MySQL connection is closed")


def endparking(LotID):
    try:
        connection = mysql.connector.connect(host='localhost',
                                             database='ParkingLotSystem',
                                             user='parkinglot',
                                             password='edgecomputing')
        cursor = connection.cursor()

        ParkingEnd = datetime.datetime.now()
        ParkingStatus = False

        mySql_insert_query = "UPDATE Cars SET ParkingStatus = 0 WHERE LotID = 1"
        val = ("noLicensePlate")
        cursor.execute(mySql_insert_query, val)
        # cursor.execute('INSERT INTO Oldparking SELECT * FROM Cars WHERE LotID = %s', LotID)
        # cursor.execute('DELETE FROM Cars WHERE LotID = 1')

        connection.commit()
        print("end")
        print(ParkingEnd)

    except mysql.connector.Error as error:
        print("Failed to insert into MySQL table {}".format(error))

    finally:
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            print("MySQL connection is closed")


def getparkingtime():
    try:
        connection = mysql.connector.connect(host='localhost',
                                             database='ParkingLotSystem',
                                             user='parkinglot',
                                             password='edgecomputing')

        cursor = connection.cursor()
        cursor.execute('SELECT ParkingStart FROM Cars WHERE LotID=1')
        records = cursor.fetchall()
        return records

    except Error as e:
        print("Error reading data from MySQL table", e)

    finally:
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            print("MySQL connection is closed")
         
         
def setoverparked():
    try:
        connection = mysql.connector.connect(host='localhost',
                                             database='ParkingLotSystem',
                                             user='parkinglot',
                                             password='edgecomputing')
        cursor = connection.cursor()

        mySql_insert_query = "UPDATE Cars SET Overparked = 1 WHERE LotID = 1"
        cursor.execute(mySql_insert_query)
        connection.commit()
        print("setoverparked")

    except mysql.connector.Error as error:
        print("Failed to insert into MySQL table {}".format(error))

    finally:
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            print("MySQL connection is closed")



         

def checkoverparked():
    parkingtime = getparkingtime()[0][0]
    timenow = datetime.datetime.now()
    timeminus30 = timenow - datetime.timedelta(1,0) # 1Minutes for testing
    if parkingtime > timeminus30:
        setoverparked()
        



serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = ""
port = 8888
print(host)
print(port)
serversocket.bind((host, port))
oldplate = "emptyplate123"
LotID = 1


class client(Thread):

    def __init__(self, socket, address):
        Thread.__init__(self)
        self.sock = socket
        self.addr = address
        self.start()

    def run(self):
        global oldplate
        while 1:
            print("Start Transmission")
            x = self.sock.recv(8192).decode()
            print(x)
            results = json.loads(x)
            print('Client sent:', results)
            if results != []: 
                if results[0] == oldplate:
                    checkoverparked()
                else:
                    setparking(results[0], 1)
                    oldplate = results[0]
                    print(oldplate)
            else:
                endparking(1)
                oldplate = "emptyplate123"
                print(oldplate)
            

serversocket.listen(5)
print('server started and listening')
while 1:
    clientsocket, address = serversocket.accept()
    client(clientsocket, address)

# insertVariblesIntoTable("MPS4400", 'TRUE', '2020-01-01 03:14:07', '2020-01-01 03:14:20', 1)
