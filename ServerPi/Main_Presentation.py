import numpy as np
import cv2
from PIL import Image
import pytesseract as tess
import time




def preprocess(img):
    # img = cv2.resize(img, (640, 480))
    # cv2.imshow("Contours",img)
    # cv2.waitKey(0)
    imgblurred = cv2.GaussianBlur(img, (5, 5), 0)
    gray = cv2.cvtColor(imgblurred, cv2.COLOR_BGR2GRAY)

    sobelx = cv2.Sobel(gray, cv2.CV_8U, 1, 0, ksize=3)
    ret2, threshold_img = cv2.threshold(sobelx, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    return threshold_img


def cleanplate(plate):
    print("CLEANING PLATE. . .")
    gray = cv2.cvtColor(plate, cv2.COLOR_BGR2GRAY)
    # kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
    # thresh= cv2.dilate(gray, kernel, iterations=1)radke@in.tum.de

    _, thresh = cv2.threshold(gray, 150, 255, cv2.THRESH_BINARY)
    contours, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    if contours:
        areas = [cv2.contourArea(c) for c in contours]
        max_index = np.argmax(areas)

        max_cnt = contours[max_index]
        # max_cntArea = areas[max_index]
        x, y, w, h = cv2.boundingRect(max_cnt)

        # if not ratiocheck(max_cntArea,w,h):
        # return plate,None

        cleaned_final = thresh[y:y + h, x:x + w]
        return cleaned_final, [x, y, w, h]

    else:
        return plate, None


def extract_contours(threshold_img):
    element = cv2.getStructuringElement(shape=cv2.MORPH_RECT, ksize=(17, 3))
    morph_img_threshold = threshold_img.copy()
    cv2.morphologyEx(src=threshold_img, op=cv2.MORPH_CLOSE, kernel=element, dst=morph_img_threshold)

    contours, hierarchy = cv2.findContours(morph_img_threshold, mode=cv2.RETR_EXTERNAL, method=cv2.CHAIN_APPROX_NONE)
    return contours


def ratiocheck(area, width, height):
    ratio = float(width) / float(height)
    if ratio < 1:
        ratio = 1 / ratio

    aspect = 4.7272
    min = 15 * aspect * 15  # minimum area
    max = 125 * aspect * 125  # maximum area

    rmin = 3
    rmax = 6

    if (area < min or area > max) or (ratio < rmin or ratio > rmax):
        return False
    return True


def ismaxwhite(plate):
    avg = np.mean(plate)
    if avg >= 115:
        return True
    else:
        return False


def validaterotationandratio(rect):
    (x, y), (width, height), rect_angle = rect

    if width > height:
        angle = -rect_angle
    else:
        angle = 90 + rect_angle

    if angle > 15:
        return False

    if height == 0 or width == 0:
        return False

    area = height * width
    if not ratiocheck(area, width, height):
        return False
    else:
        return True


def cleanandread(img, contours):
    # count=0
    for i, cnt in enumerate(contours):
        min_rect = cv2.minAreaRect(cnt)

        if validaterotationandratio(min_rect):

            x, y, w, h = cv2.boundingRect(cnt)
            plate_img = img[y:y + h, x:x + w]

            if ismaxwhite(plate_img):
                # count+=1
                clean_plate, rect = cleanplate(plate_img)

                if rect:
                    x1, y1, w1, h1 = rect
                    x, y, w, h = x + x1, y + y1, w1, h1
                    plate_im = Image.fromarray(clean_plate)
                    conf = '--psm 11 -c tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ'
                    text = tess.image_to_string(plate_im, config=conf)
                    text = text.replace('\n', '')
                    text = text.replace(' ', '')
                    print("Detected Text : ", text)
                    return text
                    # img = cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)



if __name__ == '__main__':

    image = cv2.imread("testset/20.jpg")
    threshold_img = preprocess(image)
    contours = extract_contours(threshold_img)
    platedetected = cleanandread(image, contours)

